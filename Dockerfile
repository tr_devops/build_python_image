FROM redhat/ubi8
LABEL authors="Tobias Riazi <riazi.tobias@healthnow.org>"
LABEL description="Set Version - Python"

RUN yum update -y
RUN yum install python36 python36-devel python3-setuptools python3-pip rust-toolset openssl-devel -y
RUN pip3 install setuptools-rust
RUN pip3 install wheel 
RUN pip3 install twine
RUN pip3 install pylint
RUN pip3 install pytest
RUN pip3 install coverage
RUN pip3 install chevron

RUN mkdir app
RUN chown -R 1001:0 /app

USER 1001

WORKDIR /app
